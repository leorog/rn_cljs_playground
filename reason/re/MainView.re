open BsReactNative;

let options = {
  "apiKey": "AIzaSFMjpjiUlAXJ-cp3y27yQwuHYHsYcpr1I",
  "authDomain": "teste-d9279.firebaseapp.com",
  "databaseURL": "https://teste-d9279.firebaseio.com",
  /* "projectId": "teste-d9279", */
  "storageBucket": "teste-d9279.appspot.com",
  "messagingSenderId": "1092455467576",
};

let app = BsFirebase.initializeApp(options);

let db = BsFirebase.App.database(app);

type coordinates = {
  latitude: float,
  longitude: float,
};

type business = {
  name: string,
  id: string,
  coordinates,
};

type action =
  | Click
  | GetRestaurant
  | LoadedRestaurants(array(business));

type user = {
  id: int,
  name: string,
};

type search = {business: array(business)};

type data = {search};

type json = {data};

module Decode = {
  let coordinates = json =>
    Json.Decode.{
      latitude: json |> field("latitude", float),
      longitude: json |> field("longitude", float),
    };

  let business = json =>
    Json.Decode.{
      name: json |> field("name", string),
      id: json |> field("id", string),
      coordinates: json |> field("coordinates", coordinates),
    };

  let businesses = json => Json.Decode.array(business, json);

  let search = json =>
    Json.Decode.{business: json |> field("business", businesses)};

  let data = json => Json.Decode.{search: json |> field("search", search)};

  let json = json => Json.Decode.{data: json |> field("data", data)};
};

let fetchBusiness = (lat, long) =>
  Js.Promise.(
    Fetch.fetchWithInit(
      "https://api.yelp.com/v3/graphql",
      Fetch.RequestInit.make(
        ~method_=Post,
        ~headers=
          Fetch.HeadersInit.make({
            "Content-Type": "application/graphql",
            "Authorization": "Bearer 8U2INm2EoDPqojrX-E30wb_yuY1haoRFuzlzWSWU1tN_9m4sJP9LGkhSRkoau7I5ExP2I9a8DDLYpQLgDYr3VXmOEWqJvLDfuPpPrsGgwqrBrn2XuRFITVLDlRoAXHYx",
          }),
        ~body=
          Fetch.BodyInit.make(
            {j|
                {
                  search(latitude: $lat, longitude: $long) {
                    total
                    business {
                      name
                      id
                      coordinates {
                        latitude
                        longitude
                      }
                    }
                  }
                }
        |j},
          ),
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => json |> Decode.json |> resolve)
  );
/* |> catch(_err => resolve(None)) */

type state = {
  restaurants: array(business),
  restaurant: option(business),
};

let component = ReasonReact.reducerComponent("MainView");

let make = _children => {
  ...component, /* spread the template's other defaults into here  */
  initialState: () => {restaurants: [||], restaurant: None},
  didMount: self => {
    self.send(GetRestaurant);

    ();
  },
  reducer: (action, state) =>
    switch (action) {
    | Click =>
      ReasonReact.Update({
        ...state,
        restaurant:
          Some(
            state.restaurants[Random.int(Array.length(state.restaurants))],
          ),
      })

    | GetRestaurant =>
      ReasonReact.UpdateWithSideEffects(
        state,
        (
          self => {
            Geolocation.getCurrentPosition(
              position => {
                let long: float = position##coords##longitude;

                let lat: float = position##coords##latitude;

                Js.Promise.(
                  fetchBusiness(lat, long)
                  |> then_(json =>
                       resolve(
                         self.send(
                           LoadedRestaurants(json.data.search.business),
                         ),
                       )
                     )
                );

                ();
              },
              _ => Js.log("Deu ruim"),
            );
            ();
          }
        ),
      )
    | LoadedRestaurants(restaurants) =>
      Js.log("fetchou");

      ReasonReact.Update({...state, restaurants});
    },
  render: self =>
    <View
      style=Style.(
        style([flex(1.), justifyContent(Center), alignItems(Center)])
      )>
      <Text value={js|Reason é da hora!|js} />
      /* (ReasonReact.array(Array.of_list(cards))) */
      {
        switch (self.state.restaurant) {
        | None => <Text value="Escolha o restaurante" />
        | Some(r) =>
          let regiao =
            MapView.region_t(
              ~latitude=r.coordinates.latitude,
              ~longitude=r.coordinates.longitude,
              ~latitudeDelta=0.09,
              ~longitudeDelta=0.04,
            );

          let marcardores =
            Array.map(
              r => {
                let region =
                  MapView.region_t(
                    ~latitude=r.coordinates.latitude,
                    ~longitude=r.coordinates.longitude,
                    ~latitudeDelta=0.09,
                    ~longitudeDelta=0.04,
                  );

                <Marker
                  coordinate=region
                  title={r.name}
                  onPress={
                    () =>
                      BsFirebase.Database.Reference.set(
                        BsFirebase.Database.ref(
                          db,
                          ~path="restaurants/" ++ r.id,
                          (),
                        ),
                        ~value={"name": r.name, "coordinates": r.coordinates},
                        ~onComplete=
                          a => {
                            Js.log(a);
                            ();
                          },
                        (),
                      )
                  }
                />;
              },
              self.state.restaurants,
            );

          <MapView
            region=regiao
            style=Style.(style([width(Pt(400.)), height(Pt(400.))]))>
            ...marcardores
          </MapView>;
        }
      }
      <Button
        title="Escolher restaurante"
        onPress={_event => self.send(Click)}
      />
    </View>,
};
