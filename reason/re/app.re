open BsReactNative;

/* let options = {
  "apiKey": "AIzaSFMjpjiUlAXJ-cp3y27yQwuHYHsYcpr1I",
  "authDomain": "teste-d9279.firebaseapp.com",
  "databaseURL": "https://teste-d9279.firebaseio.com",
  /* "projectId": "teste-d9279", */
  "storageBucket": "teste-d9279.appspot.com",
  "messagingSenderId": "1092455467576",
};

let app = BsFirebase.initializeApp(options);

let db = BsFirebase.App.database(app);

BsFirebase.Database.Reference.set(
  BsFirebase.Database.ref(db, ~path="teste/24", ()),
  ~value={"foo": "bar"},
  ~onComplete=
    a => {
      Js.log(a);
      ();
    },
  (),
);

type ticket = {
  id: int,
  name: string,
};

type state = {ticket};

let parseTicket = json: ticket =>
  Json.Decode.{
    id: json |> field("id", int),
    name: json |> field("name", string),
  };

BsFirebase.Database.Reference.once(
  BsFirebase.Database.ref(db, ~path="restaurants", ()),
  ~eventType="value",
  (),
)
|> Js.Promise.then_(teamDomain =>
     BsFirebase.Database.DataSnapshot.val_(teamDomain)
     |> (
       ticket => {
         Js.log(ticket);

         parseTicket(ticket)
         |> (ticketJson => Js.log(ticketJson) |> Js.Promise.resolve);
       }
     )
   ); */

let restaurants: list(RestaurantCard.restaurant) = [
  {
    name: "Café Árabe",
    lat: (-23.5744987),
    lng: (-46.690489),
    /* cuisine: Arab, */
  },
  {
    name: "Giggio",
    lat: (-23.5653385),
    lng: (-46.6836678),
    /* cuisine: Italian */
  },
  /* {name: "Paulinhos Grill", lat: 0.0, lng: 0.0, cuisine: Brazilian}, */
  /* {name: "Landi", lat: 0.0, lng: 0.0, cuisine: Brazilian}, */
  /* {name: "Alemão", lat: -27.5744987, lng: -46.690489, cuisine: Other}, */
];

let app = () => {
  Random.init(int_of_float(Js.Date.now()));

  <View
    style=Style.(
      style([flex(1.), justifyContent(Center), alignItems(Center)])
    )>
    <MainView />
  </View>;
};
